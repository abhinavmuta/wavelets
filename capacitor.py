import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from wave import diffmat
from waveguide import fd_diff
import scipy.sparse as sp


def dirichlet(A, b, bndr_pts, val):
    A[bndr_pts, :] = 0.0
    A[bndr_pts, bndr_pts] = 1.0
    b[bndr_pts] = val
    return A, b


def capacitor(D2x, D2y, title, name):
    x = np.arange(Nx)
    y = np.arange(Ny)
    xx, yy = np.meshgrid(x, y)
    xx = xx.ravel()
    yy = yy.ravel()

    Ix = np.eye(Nx)
    Iy = np.eye(Ny)
    D2 = np.kron(Iy, D2x) + np.kron(D2y, Ix) * (Nx**2.0/Ny**2.0)

    b = np.zeros(Nx*Ny)

    edges = (xx == 0) | (yy == 0) | (xx == Nx-1) | (yy == Ny-1)
    D2, b = dirichlet(D2, b, edges, val=0.0)

    plates = (xx == x0) & ((yy < y0) & (yy > y1))
    D2, b = dirichlet(D2, b, plates, val=100)

    plates = (xx == x1) & ((yy < y0) & (yy > y1))
    D2, b = dirichlet(D2, b, plates, val=-100)

    D2_sparse = sp.csr_matrix(D2)
    V = sp.linalg.spsolve(D2_sparse, b)

    V = np.reshape(V, (Nx, Ny))

    [Ex, Ey] = np.gradient(V)
    Ex = Ex
    Ey = Ey

    E = np.sqrt(Ex*Ex + Ey*Ey)

    x = (np.arange(Nx) - mpx)/Nx
    y = (np.arange(Ny) - mpy)/Nx

    plt.figure(figsize=(6, 6))
    plt.axes().set_aspect('equal', 'box')
    plt.contour(x, y, V, 100)
    plt.colorbar(fraction=0.046, pad=0.04)
    titleV = title + " of potential "
    plt.title(titleV + r"$V$")
    plt.xlim(min(x), max(x))
    plt.ylim(min(y), max(y))
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig("capacitor_V_{}.pdf".format(name), bbox_inches='tight')

    plt.figure(figsize=(6, 6))
    plt.axes().set_aspect('equal', 'box')
    plt.contour(x, y, E, 100)
    titleE = title + " of electric field "
    plt.title(titleE + r"$E$")
    plt.xlim(min(x), max(x))
    plt.ylim(min(y), max(y))
    plt.xlabel("x")
    plt.ylabel("y")
    plt.colorbar(fraction=0.046, pad=0.04)
    plt.savefig("capacitor_E_{}.pdf".format(name), bbox_inches='tight')

    # plt.contour(x, y, E)
    # plt.colorbar()
    # plt.title(title)
    # plt.quiver(x, y, Ey, Ex)
    # plt.show()
    return V


if __name__ == '__main__':
    plt.style.use("presentation")
    Nx = 100
    Ny = 100
    mpx = Nx//2
    mpy = Ny//2
    V = np.zeros([Nx, Ny])
    lp = Nx//4
    offset = lp - 1
    x0 = mpx + offset
    x1 = mpx - offset

    y0 = mpy + lp
    y1 = mpy - lp

    D2x = fd_diff(Nx, [1, -2, 1], [1, 0, -1])
    D2y = fd_diff(Ny, [1, -2, 1], [1, 0, -1])

    title="Finite Difference Solution"
    V = capacitor(D2x, D2y, title=title, name='fd')

    D2x = diffmat(2, Nx, 1, 6)
    D2y = diffmat(2, Ny, 1, 6)

    title = "Wavelet Galerkin Solution "
    V = capacitor(D2x, D2y, title=title, name='wg')
