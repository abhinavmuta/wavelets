from wave import conncoeff
import numpy as np
import matplotlib.pyplot as plt


def exact(d):
    if d < 0:
        Exception("Derivative cannot be negative")

    x = np.linspace(-np.pi, np.pi, 1000)
    if d%2 == 0:
        return (-1)**(d*0.5) * np.sin(x)
    return (-1)**(d*0.5 - 0.5) * np.cos(x)


def test_conn(d, N):
    return 0

