import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as linalg
import matplotlib.pyplot as plt
from time import time
from wave import diffmat

plt.style.use("presentation")


def fd_diff(n, coeffs, diag_idx, periodic=False):
    """
    Construct a finite difference matrix given the coefficients on the
    diagonals and their respective diagonal index

    Parameters
    ----------
    n : Int
        No of grid points.
    coeffs: Array
        Coefficients of the finite difference discretization.
    diag_idx: Array
        Indices of the diagonal of the respective coefficients, e.g. 0 if on
        principle diagonal, `1` if above the principle diagonal, `-1` if below.
    """
    D = np.zeros([n, n])
    for i in range(len(coeffs)):
        j = n - abs(diag_idx[i])
        D += np.diag([coeffs[i]] * j, diag_idx[i])
        if periodic:
            idx = diag_idx.index(-diag_idx[i])
            if diag_idx[idx] < 0:
                j = -n - diag_idx[idx]
            D += np.diag([coeffs[idx]] * abs(diag_idx[idx]), j)
    return D


def solve(Ix, Iy, D2x, D2y, name, plot):
    dy = dx
    D = -np.kron(Ix, D2x) - np.kron(D2y, Iy) *(dx**2.0/dy**2.0)
    D_sparse = sp.csc_matrix(D)


    # plt.spy(D, markersize=4)
    # plt.show()
    start = time()
    h, V = linalg.eigs(D_sparse, k=4, which='SM')
    ttook = time() - start
    print("Elapsed time: {}s".format(ttook))


    print("Cutoff Wavenumber (Numerical):\n")
    print(np.sqrt(h)/dx)
    h = abs(np.sqrt(h)/dx)

    if plot:
        fig, ax = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(6, 6))
        ax = ax.ravel()
        for i in range(4):
            V1 = np.reshape(V[:, i], (Ny-2, Nx-2))
            cax = ax[i].contourf(x, y, abs(V1))
            fig.colorbar(cax, ax=ax[i], format="%.0e")
            ax[i].set_title("eig = {:.3f}".format(h[i]))

        plt.savefig("eig_{}.pdf".format(name), bbox_inches='tight')
    return h, V, ttook

def norm(f, g):
    l2norm = np.sqrt(np.sum(abs(f*f - g*g)))/len(f)
    return l2norm


def main(plot):
    ah = []
    for i in [1, 2]:
        for j in [1, 2]:
            ah += [np.sqrt((i*np.pi/(a-2*dx))**2.0 + (j*np.pi/(b-2*dy))**2.0)]
    print("Analytical eigen values are \n{}".format(ah))

    D2x = fd_diff(Nx, [1, -2, 1], [1, 0, -1])
    D2y = fd_diff(Ny, [1, -2, 1], [1, 0, -1])

    # Interior points
    D2x = D2x[1:Nx-1, 1:Nx-1]
    D2y = D2y[1:Ny-1, 1:Ny-1]


    Ix = np.eye(Ny-2)
    Iy = np.eye(Nx-2)
    h_fd, V_fd, ttook1 = solve(Ix, Iy, D2x, D2y, name='fd', plot=plot)

    D2x = diffmat(2, Nx, a, 10)
    D2y = diffmat(2, Ny, b, 10)

    # Interior points
    D2x = D2x[1:Nx-1, 1:Nx-1] * dx**2
    D2y = D2y[1:Ny-1, 1:Ny-1] * dx**2

    h_wg, V_wg, ttook2 = solve(Ix, Iy, D2x, D2y, name='wg', plot=plot)

    if plot:
        error = []
        for i in range(4):
            error += [norm(V_wg[:, i], V_fd[:, i])]
        plt.figure(figsize=(8, 8))
        plt.semilogy(error)
        plt.title("Error b/w FD and WG eigenvectors")
        plt.axes().set_aspect('equal', 'box')
        plt.savefig("eig_error.pdf", bbox_inches='tight')
    return ttook1, ttook2


if __name__ == '__main__':
    t1 = []
    t2 = []
    a = 2
    b = 1
    Dx = np.arange(0.02, 0.015, -0.005)
    for dx in Dx:
        print(dx)
        dy = dx
        x = np.arange(0, a, dx)
        y = np.arange(0, b, dy)

        Nx = int(a/dx) + 1 - 2 + 3
        Ny = int(b/dy) + 1 - 2 + 3
        t = main(plot=False)
        t1.append(t[0])
        t2.append(t[1])
    t1 = np.array(t1)
    t2 = np.array(t2)
    plt.semilogx(Dx, t2/t1, label='wg/fd')
    plt.title("Relative time taken to compute the eigensolution")
    plt.legend()
    plt.savefig("eig_time.pdf", bbox_inches='tight')
