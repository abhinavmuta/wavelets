import numpy as np
import scipy.signal as sg


def gaussian(iters=10):
    y0 = np.ones(1000)
    y = y0
    for i in range(iters):
        y = sg.fftconvolve(y, y0)
    norm = np.trapz(y, dx=np.pi/(len(y)))
    y = y/norm
    t = np.linspace(-np.pi*0.5, np.pi*0.5, len(y))
    return t, y


def wavelet(alp=[], ahp=[], slp=None, shp=None, conjugate=True):
    """Give the Analysis and Synthesis filter coefficients
    """
    if conjugate:
        slp = ahp
        shp = alp

    x = np.linspace(0, 100, 10)
    phi = []
    psi = []
    return x, phi, psi


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    t, phi = scaling_func(2, 10)
    plt.plot(t, phi)
    plt.show()

    # Gaussian from repeated convolutions of impluse func with it self.
    for i in range(0, 100, 10):
        t, y = gaussian(i)
        plt.plot(t, y)
    plt.show()

