from PIL import Image
import numpy as np
from matplotlib import pyplot as plt


def dwt(image):
    l = (image[:-1, :] + image[1:, :]) * 0.5
    l = l[::2]

    h = (image[:-1, :] - image[1:, :]) * 0.5
    h = h[::2]

    ll = (l[:, :-1] + l[:, 1:]) * 0.5
    ll = ll[:, ::2]

    lh = (l[:, :-1] - l[:, 1:]) * 0.5
    lh = lh[:, ::2]

    hl = (h[:, :-1] + h[:, 1:]) * 0.5
    hl = hl[:, ::2]

    hh = (h[:, :-1] - h[:, 1:]) * 0.5
    hh = hh[:, ::2]
    return ll, lh, hl, hh


def dwtwrapper(image, num=1):
    if num<1:
        return "Num more than 1 required"
    tile = [image]
    for i in range(num):
        ll, lh, hl, hh = dwt(image)
        image = ll
        tile.append(lh)
        tile.append(hh)
        tile.append(hl)
        tile.append(ll)

    for i in range(num):
        val = 4*(num-i)
        stack = make_tile(tile[val-3], tile[val-2], tile[val-1], tile[val])
        tile[val -4] = stack

    return tile[0]


def make_tile(lh, hh, hl ,ll):
    im1 = np.vstack([ll, lh])
    im2 = np.vstack([hl, hh])
    im = np.hstack([im1, im2])
    return im

def plot(image, lvl=1):
    plt.imshow(image , cmap='gray')
    plt.axis('off')
    plt.show()



if __name__ == '__main__':
    lena = np.array(Image.open("woman.jpg"))
    tile = dwtwrapper(lena, num=2)
    plot(tile)



