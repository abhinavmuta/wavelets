import numpy as np
import matplotlib.pyplot as plt
from wave import diffmat
from waveguide import fd_diff
import scipy.sparse as sp


def dirichlet(A, b, bndr_pts, val):
    A[bndr_pts, :] = 0.0
    A[bndr_pts, bndr_pts] = 1.0
    b[bndr_pts] = val
    return A, b


def neumann(A, b, D, bndr_pts, val):
    A[bndr_pts, :] = D[bndr_pts, :]
    b[bndr_pts] = val
    return A, b


def diffusion(D2x, D2y, D):
    x = np.arange(Nx)
    y = np.arange(Ny)
    xx, yy = np.meshgrid(x, y)
    xx = xx.ravel()
    yy = yy.ravel()

    Ix = np.eye(Nx)
    Iy = np.eye(Ny)
    D2 = np.kron(Iy, D2x) + np.kron(D2y, Ix)
    D = np.kron(Iy, Dx)

    b = np.zeros(Nx*Ny)

    edges = (yy == 0) | (yy == Ny-1)
    D2, b = dirichlet(D2, b, edges, val=0.0)

    edges = (xx == 0)
    D2, b = dirichlet(D2, b, edges, val=1.0)

    edges = (xx == Nx)
    D2, b = neumann(D2, b, D, edges, val=0.0)

    plt.spy(D2, markersize=4)
    plt.show()

    D2_sparse = sp.csr_matrix(D2)
    U = sp.linalg.spsolve(D2_sparse, b)

    U = np.reshape(V, (Ny, Nx))

    x = np.arange(Nx) - mpx
    y = np.arange(Ny) - mpy

    plt.contour(U)
    plt.xlim(min(x), max(x))
    plt.ylim(min(y), max(y))
    # plt.colorbar()
    plt.show()
    return U


if __name__ == '__main__':
    Nx = 101
    Ny = 101
    mpx = Nx//2
    mpy = Ny//2
    V = np.zeros([Nx, Ny])
    lp = Nx//4
    offset = lp - 10
    x0 = mpx + offset
    x1 = mpx - offset

    y0 = mpy + lp
    y1 = mpy - lp

    D2x = fd_diff(Nx, [1, -2, 1], [1, 0, -1])
    D2y = fd_diff(Ny, [1, -2, 1], [1, 0, -1])
    Dx = fd_diff(Nx, [1, -1], [0, -1])
    D2x = D2x * Nx**2.0
    D2y = D2y * Ny**2.0

    V = diffusion(D2x, D2y, Dx)

    D2x = diffmat(2, Nx, 1, 6)
    D2y = diffmat(2, Ny, 1, 6)
    Dx = diffmat(1, Nx, 1, 6)

    V = diffusion(D2x, D2y, Dx)
