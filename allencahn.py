"""
Allen-Cahn equation is given by
`u_t = u_xx + u - u^3`
where,
`u(-1) = -1` and `u(1) = 1`
"""
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as linalg
import matplotlib.pyplot as plt
from time import time
from wave import diffmat


if __name__ == '__main__':
    N = 20
    x = np.linspace(0, 1, N)
    D2x = diffmat(2, N, 1, 6)
    # interior points
    D2x[[0, N], :] = np.zeros(1, N)
    eps = 0.01
    dt = min([0.01, 50*N**(-4)/eps])
    t = 0
    v = 0.53*x + 0.47*np.sin(-1.5*np.pi*x)


    tmax = 100
    tplot = 2
    nplots = np.round(tmax/tplot)
    plotgap = np.round(tplot/dt)
    dt = tplot/plotgap
    xx = np.arange(-1, 1, 0.025)
    vv = np.polyval(np.polyfit(x, v, N), xx)
    plotdata = np.concatenate([vv, np.zeros(nplots, len(xx))])
    tdata = t

    for i in range(nplots):
        for j in range(plotgap):
            t += dt
            v = v + dt*(eps*D2x@(v-x) + v - v**3.0)

        vv = np.polyval(np.polyfit(x, v, N), xx)
        plotdata[i+1, :] = vv
        tdata = np.concatenate([tdata, t])


    start = time()
    end = time()
