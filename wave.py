import numpy as np
from numpy import sqrt
from scipy.signal import daub, fftconvolve
from scipy.special import binom, factorial


def daubhp(N):
    """
    Calculate high pass coefficeints given low pass coefficients

    Parameters
    ----------
    N : int
        Genus of the daubechies wavelet

    Returns
    -------
    hp : ndarray
        High pass coefficients of Daubechies wavelet

    References
    ----------
    .. [1] G. Beylkin (1992) "On the Representation of Operators in Bases of
    Compactly Supoorted Wavelets." SIAM Journal on Numerical Analysis 29:6, pp.
    1716-1740
    """
    lp = daub(N//2)
    hp = np.zeros_like(lp)
    for i in range(N):
        hp[i] = (-1)**i * lp[N-i-1]
    return hp


def scaling_func(genus=4, q=2, hp=False):
    """
    Construct Daubechies scaling function given genus and number of iterations,
    using repeated convolving of the low pass filter coefficients.

    Parameters
    ----------
    genus : int
        Genus of the daubechies scaling function
    iters : int
        No of iters to convolve
    """
    # lp = daub(genus//2)
    lp = daubhp(genus) if hp else daub(genus//2)
    coeffs = lp*np.sqrt(2)
    phi_new = coeffs
    phi = [1]
    for i in range(1, 15):
        phi = fftconvolve(phi, phi_new)
        phi_new = np.zeros(2*len(phi_new) - 1)
        phi_new[1::2**i] = coeffs

    t = np.linspace(0, len(coeffs) - 1, len(phi)) / 2**(q)
    psi = np.zeros_like(phi)

    phi = phi[1:-1:2] * hp
    return t, phi, psi


def moments(deg, genus):
    """
    Compute moments of scaling function given low pass coefficients and the
    number of moments

    Parameters
    ----------
    deg : Int
        Highest degree of the polynomial for which moments should satisfy
    genus : Int
        Genus of the daubechies wavelet

    Returns
    -------
    mom : ndarray
        The moments of scaling function with zero translation
    """
    if deg < 0:
        raise Exception("Polynomial degree cannot be less than 0")

    if deg > genus:
        raise Exception("Polynomial degree cannot be greater than Genus of")

    lp = daub(genus//2)
    mom = np.zeros(deg+1)
    mom[0] = 1

    for d in range(1, deg+1):
        fac = 1/sqrt(2)/(2**d - 1)
        for i in range(d):
            tmp = 0
            for j in range(genus):
                tmp += lp[j] * j**(d - i)
            mom[d] += fac * binom(d, i) * mom[i] * tmp
    return mom


def tmoments(l, deg, genus):
    """
    Calucalate the moments of translated scaling function given the
    translated parameter `l`

    Parameters
    ----------
    l : Int
        Translation parameter, i.e, moments are calulated for this translated
        wavelet
    deg : Int
        Highest degree of the polynomial for which moments should satisfy
    genus : Int
        Genus of the daubechies wavelet

    Return
    -------
    tmom : ndarray
        The moments of any translated scaling function.
    """
    mom = moments(deg=deg, genus=genus)
    tmom = np.zeros(deg+1)
    for d in range(deg+1):
        for j in range(d+1):
            tmom[d] += binom(d, j) * mom[j] * l**(d-j)
    return tmom


def conncoeff(order=2, genus=6, basis='SS'):
    """
    Two term connection coefficients for Daubechies scaling function.

    Parameters
    ----------
    order : int
        Order of the derivative for which connection coefficients are required
    genus : int
        Genus of the daubechies wavelet, should be a even number
    basis : str
        Specify the trial and test functions of the Wavelet-Galerkin problem.
        'SS' : both trial and test functions are scaling functions
        'SW' : test function is scaling function and trial function is wavelet
        'WS' : test function is wavelet function and trial function is scaling
            function
        'WW' : both trial and test functions are wavelets

    Returns
    -------
    Gamma : ndarray
        Connection coeffiecient matrix for `d` order deriative in `N` genus
        wavelet basis

    Notes
    -----
    After constructing `A` matrix find the eigen values of `A`, since
    `Ax = 0` where `x` is the coefficient matrix. `x` is in the null space of
    `A` so find the eigen vector corresponding to the eigen value `0` and
    normalize the resulting eigen vector using moment equation
    `$\sigma M_{l}^{d}\Gamma = d!$`.

    References
    ----------
    .. [2] Ole Moeller Nielsen (1998) "Wavelets in Scienticfic Computing"
    Technical University of Denmark
    """
    lp = daub(genus//2)
    hp = daubhp(genus)

    if basis == 'SS':
        cf1 = lp
        cf2 = lp
    elif basis == 'SW':
        cf1 = lp
        cf2 = hp
    elif basis == 'WS':
        cf1 = hp
        cf2 = lp
    elif basis == 'WW':
        cf1 = hp
        cf2 = hp

    M = 2*(genus-2) + 1
    A = np.zeros([M, M])
    for i in range(M):
        for j in range(M):
            l = i - (genus - 2)
            q = j - (genus - 2)
            for k in range(genus):
                qq = k + q - 2*l
                if (qq >=0) and (qq <=genus-1):
                    A[i, j] += cf1[k]*cf2[qq]
    A = A - np.eye(M)/2**(order)

    Mrow = np.zeros(M)
    for i in range(M):
        l = i - (genus - 2)
        tmom = tmoments(l, order, genus)
        Mrow[i] = tmom[order]

    U, D, V = np.linalg.svd(A)
    V = V.T
    Gamma = V[:, M-1]
    Gamma = Gamma.ravel()
    c = (-1)**order * factorial(order)/(np.dot(Mrow, Gamma))
    Gamma = c*Gamma
    return Gamma


def diffmat_n(order, N, L, genus):
    """
    Differentiation matrix as given in [2], only for periodic domains.

    Parameters
    ----------
    order : int
        Order of the differentiation matrix.
    N : int
        Number of grid points in the domain.
    L : int
        Length of the domain.
    genus : int
        Genus of the daubechies sacling function / wavelet.

    Returns
    -------
    DM : array
        Differentiation matrix with givem parameters


    References
    ----------
    .. [2] Ole Moeller Nielsen (1998) "Wavelets in Scienticfic Computing"
    Technical University of Denmark
    """
    Gamma = conncoeff(order, genus)
    Gamma = ((N/L)**order)*Gamma

    nn = N
    while nn <= genus - 2:
        nn = 2*nn

    DM = np.zeros([N, N])
    for i in range(N):
        for j in range(-genus+2, genus-2+1):
            k = (nn+i-j) % N + 1
            DM[i, k-1] += Gamma[j+genus-2]
    return DM.T


def diffmat(order, N, L, genus, basis='SS', periodic=False):
    """
    Differentiation matrix as given in [2], can handle both periodic as well
    as non-periodic domains.

    Parameters
    ----------
    order : int
        Order of the differentiation matrix.
    N : int
        Number of grid points in the domain.
    L : int
        Length of the domain.
    genus : int
        Genus of the daubechies sacling function / wavelet.
    periodic : bool
        Specifies wether a domain is periodic or not.
    basis : str
        Specify the trial and test functions of the Wavelet-Galerkin problem.a
        'SS' : both trial and test functions are scaling functions
        'SW' : test function is scaling function and trial function is wavelet
        'WS' : test function is wavelet function and trial function is scaling
            function
        'WW' : both trial and test functions are wavelets

    Returns
    -------
    DM : array
        Differentiation matrix with givem parameters

    TODO
    ----
    Make use of scipy.sparse.diags to construct the matrix more efficiently.

    References
    ----------
    .. [2] Ole Moeller Nielsen (1998) "Wavelets in Scienticfic Computing"
    Technical University of Denmark
    """
    Gamma = conncoeff(order, genus, basis=basis)
    Gamma = ((N/L)**order)*Gamma

    # Indices of the diagonals correspondint to different Gamma
    idx = np.arange(len(Gamma)) - len(Gamma)//2

    DM = np.zeros([N, N])
    for i in range(len(Gamma)):
        # j is the number of Gamma[i]'s in a given diagonal
        j = N - abs(idx[i])
        DM += np.diag([Gamma[i]] * j, idx[i])

        # remove the idx == 0 index as it return empty array
        if periodic and idx[i] != 0:
            k = np.sign(idx[i]) * N - idx[i]
            DM += np.diag([Gamma[i]] * abs(idx[i]), k)
    return DM.T


if __name__ == "__main__":
    nop = 1<<12
    x = np.linspace(0, 1, nop, endpoint=True)
    c = 400

