import numpy as np
import matplotlib.pyplot as plt
from wave import diffmat
from waveguide import fd_diff
import scipy.sparse as sp


def dirichlet(A, b, bndr_pts, val):
    A[bndr_pts, :] = 0.0
    A[bndr_pts, bndr_pts] = 1.0
    b[bndr_pts] = val
    return A, b


def capacitor(D2x, D2y):
    x = np.arange(Nx)
    y = np.arange(Ny)
    xx, yy = np.meshgrid(x, y)
    xx = xx.ravel()
    yy = yy.ravel()

    Ix = np.eye(Nx)
    Iy = np.eye(Ny)
    D2 = np.kron(Iy, D2x) + np.kron(D2y, Ix) * (Nx**2.0/Ny**2.0)

    b = np.zeros(Nx*Ny)

    edges = (xx == 0) | (yy == 0) | (xx == Nx-1)
    D2, b = dirichlet(D2, b, edges, val=0.0)

    edges =  (yy == Ny-1)
    D2, b = dirichlet(D2, b, edges, val=1.0)

    D2_sparse = sp.csr_matrix(D2)
    V = sp.linalg.spsolve(D2_sparse, b)

    V = np.reshape(V, (Nx, Ny))

    xx = np.linspace(0, 1, Nx)
    yy = np.linspace(0, 1, Ny)

    # plt.contour(x, y, V, np.arange(-101, 101, 0.5))
    plt.subplot(1, 2, 1)
    plt.contourf(xx, yy, V)
    plt.colorbar()


    x, y = np.meshgrid(xx, yy)
    A = np.zeros((Nx, Ny))
    for n in range(1, 100, 2):
        A += (4/(n*np.pi) * (np.sin(n * np.pi*x)
                                    * np.sinh(n * np.pi*y))/
                        (np.sinh(n*np.pi)))
    plt.subplot(1, 2, 2)
    plt.contourf(xx, yy, A)
    plt.colorbar()
    plt.show()


    B = V.ravel()
    A = A.ravel()
    error = np.sqrt(sum((B - A) **2.0)) / len(V)
    print("Error is: ", error)
    return V


if __name__ == '__main__':
    Nx = 100
    Ny = 100
    mpx = Nx//2
    mpy = Ny//2
    V = np.zeros([Nx, Ny])
    lp = Nx//4
    offset = lp - 1
    x0 = mpx + offset
    x1 = mpx - offset

    y0 = mpy + lp
    y1 = mpy - lp

    D2x = fd_diff(Nx, [1, -2, 1], [1, 0, -1])
    D2y = fd_diff(Ny, [1, -2, 1], [1, 0, -1])

    # V = capacitor(D2x, D2y)

    D2x = diffmat(2, Nx, 1, 6, basis='SS')
    D2y = diffmat(2, Ny, 1, 6, basis='SS')

    V = capacitor(D2x, D2y)

    D2x = diffmat(2, Nx, 1, 6, basis='WW')
    D2y = diffmat(2, Ny, 1, 6, basis='WW')

    W = capacitor(D2x, D2y)

    plt.contourf(V+W)
    plt.colorbar()
    plt.show()
